AM_CFLAGS = $(CFLAGS) -Wall

# Link shared.o and libstatic.a to form the shared library, libshared.so.
libshared.so: shared.o libstatic.a
	cc -shared -o $@ $^

# Package static.o into an archive to form libstatic.a.
libstatic.a: static.o
	ar rcs $@ $^

# Debug information dump.
debug.log: libstatic.a shared.o
	readelf --relocs $^ > $@
	readelf --symbols $^ >> $@
	readelf --sections $^ >> $@
	objdump --disassemble $^ >> $@
	objdump --all-headers $^ >> $@

# Generic C compilation rules, not specific to static or shared compilation.
# Note that -fPIC is not passed in.
%.o: %.c
	cc $(AM_CFLAGS) -c -o $@ $^
%.o: %.c %.h

# Generic clean rules.
clean:
	rm -f libshared.so shared.o
	rm -f libstatic.a static.o
maintainer-clean: clean
	rm debug.log

.PHONY: clean maintainer-clean
